#!/bin/bash

set -e

# 颜色定义
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

LATEST_VERSION="v20.7.0"

# 加载动画
spinner() {
    local pid=$1
    local text=$2
    local spinstr='⣾⣽⣻⢿⡿⣟⣯⣷'
    while ps -p $pid > /dev/null; do
        for i in ${spinstr}; do
            echo -ne "\r$text $i"
            sleep 0.1
        done
    done
    echo -ne "\r\033[K"
}

# 检测系统架构
detect_architecture() {
    case "$(uname -m)" in
        x86_64) NODE_ARCH="x64" ;;
        aarch64|arm64) NODE_ARCH="arm64" ;;
        *) echo -e "${RED}不支持的架构: $(uname -m)${NC}"; exit 1 ;;
    esac
}

# 下载安装Node.js
install_nodejs() {
    if ! command -v node &> /dev/null; then
        detect_architecture
        local url="https://nodejs.org/dist/$LATEST_VERSION/node-$LATEST_VERSION-linux-$NODE_ARCH.tar.xz"
        local tarball="node-$LATEST_VERSION-linux-$NODE_ARCH.tar.xz"
        
        # 下载
        curl -L -s "$url" -o "$tarball" &
        spinner $! "正在下载Node.js"
        echo -e "${GREEN}✓${NC}"
        
        # 解压安装
        tar -xf "$tarball" &
        spinner $! "正在解压安装"
        
        sudo mkdir -p /opt/node
        sudo mv "node-$LATEST_VERSION-linux-$NODE_ARCH"/* /opt/node/
        sudo ln -sf /opt/node/bin/node /usr/local/bin/node
        sudo ln -sf /opt/node/bin/npm /usr/local/bin/npm
        
        # 清理
        rm -f "$tarball"
        rm -rf "$HOME/node-$LATEST_VERSION-linux-$NODE_ARCH/"
        echo -e "${GREEN}✓${NC}"
        
        echo 'export PATH=$PATH:$(npm config get prefix)/bin' >> "$HOME/.profile"
        source $HOME/.profile
    fi
}

# 配置npm
setup_npm() {
    if command -v npm &> /dev/null; then
        npm config set registry https://registry.npmmirror.com &
        spinner $! "配置npm镜像"
        echo -e "${GREEN}✓${NC}"
    fi
}

# 验证安装
verify() {
    if command -v node &> /dev/null && command -v npm &> /dev/null; then
        echo -e "${GREEN}Node.js $(node -v) 和 npm $(npm -v) 安装完成${NC}"
    else
        echo -e "${RED}安装失败${NC}"
        exit 1
    fi
}

# 主函数
main() {
    install_nodejs
    setup_npm
    verify
}

main