#!/bin/bash

set -e

# 颜色定义
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

# 加载动画
spinner() {
    local pid=$1
    local text=$2
    local spinstr='⣾⣽⣻⢿⡿⣟⣯⣷'
    while ps -p $pid > /dev/null; do
        for i in ${spinstr}; do
            echo -ne "\r$text $i"
            sleep 0.1
        done
    done
    echo -ne "\r\033[K"
}

# 安装pnpm
install_pnpm() {
    if ! command -v pnpm &>/dev/null; then
        source <(curl -sL "https://raw.gitcode.com/Xrkseek/sunflower-yunzai-scripts/raw/master/shell_modules/github.sh")
        
        cd "$HOME" || { echo -e "${RED}无法进入HOME目录${NC}"; exit 1; }
        
        # 配置版本和架构
        local version="v9.15.3"
        local arch=$(uname -m)
        local binary_name
        local url
        
        case "$arch" in
            x86_64)
                binary_name="pnpm-linux-x64"
                url="https://github.com/pnpm/pnpm/releases/download/$version/pnpm-linux-x64"
                ;;
            aarch64)
                binary_name="pnpm-linux-arm64"
                url="https://github.com/pnpm/pnpm/releases/download/$version/pnpm-linux-arm64"
                ;;
            *)
                echo -e "${RED}不支持的架构: $arch${NC}"
                exit 1
                ;;
        esac

        # 获取下载地址
        getgh url || url="https://github.moeyy.xyz/$url"
        
        # 下载pnpm
        wget -q --tries=3 --timeout=30 -O "$binary_name" "$url" &
        spinner $! "正在下载pnpm"
        
        if [ $? -eq 0 ]; then
            echo -e "${GREEN}✓${NC}"
            
            # 安装pnpm
            sudo mv "$HOME/$binary_name" /usr/local/bin/pnpm &
            spinner $! "正在安装pnpm"
            sudo chmod 755 /usr/local/bin/pnpm
            
            # 验证安装
            if pnpm -v &>/dev/null; then
                echo -e "${GREEN}pnpm $(pnpm -v) 安装完成${NC}"
                pnpm config set node_sqlite3_binary_host_mirror https://npmmirror.com/mirrors/sqlite3
            else
                echo -e "${RED}安装失败${NC}"
                exit 1
            fi
        else
            echo -e "${RED}下载失败，请检查网络${NC}"
            exit 1
        fi
    fi
}

install_pnpm