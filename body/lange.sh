#!/bin/bash

# 获取系统架构
ARCH=$(uname -m)
echo "Architecture: $ARCH"  # 调试输出

# 获取 C 库类型
if [ -f /lib/x86_64-linux-gnu/libc.so.6 ]; then
  LIBC="glibc"
elif [ -f /lib/x86_64-linux-musl/libc.so.1 ]; then
  LIBC="musl"
else
  LIBC=""
fi

echo "C Library: $LIBC"  # 调试输出

if [[ "$ARCH" == "x86_64" && "$LIBC" == "glibc" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-x64_net8.0_SelfContained.tar.gz"
elif [[ "$ARCH" == "x86_64" && "$LIBC" == "musl" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-musl-x64_net8.0_SelfContained.tar.gz"
elif [[ "$ARCH" == "aarch64" && "$LIBC" == "glibc" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-arm64_net8.0_SelfContained.tar.gz"
elif [[ "$ARCH" == "aarch64" && "$LIBC" == "musl" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-musl-arm64_net8.0_SelfContained.tar.gz"
elif [[ "$ARCH" == "armv7l" && "$LIBC" == "glibc" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-arm_net8.0_SelfContained.tar.gz"
elif [[ "$ARCH" == "armv7l" && "$LIBC" == "musl" ]]; then
  echo "Recommended version: Lagrange.OneBot_linux-musl-arm_net8.0_SelfContained.tar.gz"
else
  echo "Unknown architecture or libc type. Please check your system manually."
fi