# 前景色（文字颜色）
export color_pink="\e[1;32m"
export color_red="\033[31m"
export color_yellow="\033[33m"
export color_cyan="\e[96m"
export color_light_cyan="\033[1;96m"
export color_light_green="\033[1;92m"
export color_white="\033[0;97m"
export color_light_blue="\e[1;36m"
export color_light_yellow="\033[1;93m"

# 背景色
export bg="\e[0m"
export bg_default="\e[49m"
export bg_black="\033[40m"
export bg_red="\033[41m"
export bg_green="\033[42m"
export bg_yellow="\033[43m"
export bg_blue="\033[44m"
export bg_magenta="\033[45m"
export bg_cyan="\033[46m"
export bg_white="\033[47m"

# 亮背景色
export bg_bright_black="\033[100m"
export bg_bright_red="\033[101m"
export bg_bright_green="\033[102m"
export bg_bright_yellow="\033[103m"
export bg_bright_blue="\033[104m"
export bg_bright_magenta="\033[105m"
export bg_bright_cyan="\033[106m"
export bg_bright_white="\033[107m"

# 加粗文字颜色
export bold_black="\033[1;30m"
export bold_red="\033[1;31m"
export bold_green="\033[1;32m"
export bold_yellow="\033[1;33m"
export bold_blue="\033[1;34m"
export bold_magenta="\033[1;35m"
export bold_cyan="\033[1;36m"
export bold_white="\033[1;37m"

# 重置颜色
export reset_color="\033[0m"
export reset_all="\033[0m"