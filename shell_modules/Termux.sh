#!/data/data/com.termux/files/usr/bin/bash

FONT_URL="https://raw.gitcode.com/Xrkseek/sunflower-yunzai-scripts/raw/master/Termux-container"
#修改键盘和字体布局
function 字体键盘魔法() {
    local font_dir="$HOME/.termux"
    local font_file="$font_dir/font.ttf"
    local prop_file="$font_dir/termux.properties"
    local font_url="${FONT_URL}/font.ttf"
    local prop_url="${FONT_URL}/termux.properties"


    mkdir -p "$font_dir"

    local new_font_hash=$(curl -s "$font_url" | sha256sum | awk '{print $1}')
    local new_prop_hash=$(curl -s "$prop_url" | sha256sum | awk '{print $1}')
    local existing_font_hash
    local existing_prop_hash
    if [ -f "$font_file" ]; then
        existing_font_hash=$(sha256sum "$font_file" | awk '{print $1}')
    fi
    if [ -f "$prop_file" ]; then
         existing_prop_hash=$(sha256sum "$prop_file" | awk '{print $1}')
    fi


    if [ -n "$existing_font_hash" ] && [ -n "$existing_prop_hash" ] && [ "$existing_font_hash" == "$new_font_hash" ] && [ "$existing_prop_hash" == "$new_prop_hash" ]; then
        echo "字体和键盘布局已存在且为最新，无需重新设置。"
        return
    fi

    echo "下载字体文件..."
    curl -L -o "$font_file" "$font_url"
    echo "下载键盘布局..."
    curl -L -o "$prop_file" "$prop_url"
    termux-reload-settings
    echo "字体和键盘布局已设置完成。"
}
#换源与安装必备软件包
function 换源魔法() {
    bash <(curl -sL https://raw.gitcode.com/Xrkseek/sunflower-yunzai-scripts/raw/master/Termux-container/repo.sh)
    yes | apt update -y || { echo "更新失败，请检查网络连接"; exit 1; }
    for pkg in tar proot wget git; do
        if ! command -v "$pkg" &>/dev/null; then
            pkg install -y "$pkg"
        fi
    done
}
