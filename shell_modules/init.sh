#!/bin/bash

# 初始化变量
function check_changes {
    export tyz=""
    export myz=""
    export xyz=""
    export yz=""
    search_root="$HOME"
}

# 检查指定目录中的 package.json
function check_directory {
    local dir="$1"
    local package_file="$dir/package.json"
    
    # 如果文件存在
    if [[ -f "$package_file" ]]; then
        local name=$(cat "$package_file" | jq -r '.name // empty' 2>/dev/null)
        
        # 如果能成功获取到name字段
        if [[ ! -z "$name" ]]; then
            case "$name" in
                "trss-yunzai")
                    if [[ -z "$tyz" ]]; then
                        export tyz="$dir"
                        export yz="$tyz"
                    elif [[ -z "$yz" ]]; then
                        export yz="$dir"
                    fi
                    return 0
                    ;;
                "miao-yunzai" | "yunzai")
                    if [[ -z "$myz" ]]; then
                        export myz="$dir"
                        export yz="$myz"
                    elif [[ -z "$yz" ]]; then
                        export yz="$dir"
                    fi
                    return 0
                    ;;
                "xrk-yunzai")
                    if [[ -z "$xyz" ]]; then
                        export xyz="$dir"
                        export yz="$xyz"
                    elif [[ -z "$yz" ]]; then
                        export yz="$dir"
                    fi
                    return 0
                    ;;
                
            esac
        fi
    fi
    return 1
}

# 优先搜索常见目录
function search_common_paths {
    local common_paths=(
        "$HOME/Yunzai"
        "$HOME/Miao-Yunzai"
        "$HOME/TRSS-Yunzai"
        "$HOME/XRK-Yunzai"
    )
    
    for path in "${common_paths[@]}"; do
        if check_directory "$path"; then
            return 0
        fi
    done
    return 1
}

# 扩展搜索所有目录
function search_all_directories {
    find "$search_root" -type f -name "package.json" ! -path "*/node_modules/*" -print0 | while IFS= read -r -d $'\0' package_file; do
        if check_directory "$(dirname "$package_file")"; then
            return 0
        fi
    done
}

# 让用户选择想要操作的目录
function prompt_user_to_select {
    echo "检测到多个匹配的安装，请选择要操作的目录："
    local options=()
    local i=1

    # 如果有多个选项，将目录存储进数组
    if [[ -n "$tyz" ]]; then
        options+=("$i. 时雨崽目录: $tyz")
        ((i++))
    fi
    if [[ -n "$myz" ]]; then
        options+=("$i. 喵崽目录: $myz")
        ((i++))
    fi
    if [[ -n "$xyz" ]]; then
        options+=("$i. 葵崽目录: $xyz")
        ((i++))
    fi


    # 打印选项并接受用户选择
    PS3="请输入你的选择："
    select opt in "${options[@]}"; do
        case $opt in
            "${options[0]}")
                export yz="$tyz"
                break
                ;;
            "${options[1]}")
                export yz="$myz"
                break
                ;;
            "${options[3]}")
                export yz="$xyz"
                break
                ;;
            *)
                echo "无效选项，请重新选择"
                ;;
        esac
    done
}

# 主搜索函数
function search_directories {
    if ! search_common_paths; then
        search_all_directories
    fi
    if [[ -n "$tyz" && -n "$myz" && -n "$xyz" ]]; then
        prompt_user_to_select
    fi
}